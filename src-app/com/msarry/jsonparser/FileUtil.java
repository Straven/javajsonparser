package com.msarry.jsonparser;

import com.msarry.jsonparser.core.DataCharBuffer;

import java.io.FileReader;
import java.io.IOException;

public class FileUtil {
    public static DataCharBuffer readFile(String filename) throws IOException {
        FileReader reader = new FileReader(filename);
        DataCharBuffer dataCharBuffer = new DataCharBuffer(8192);
        dataCharBuffer.length = reader.read(dataCharBuffer.data);
        reader.close();
        return dataCharBuffer;
    }
}
