package com.msarry.jsonparser;

import com.msarry.jsonparser.core.DataCharBuffer;

import java.io.IOException;

public class IterationBenchmark {
    public static void main(String[] args) throws IOException {
        String filename = "data/small.json.txt";
        if(args.length > 0) {
            filename = args[0];
        }
        System.out.println("iterating: " + filename);

        DataCharBuffer dataCharBuffer = FileUtil.readFile(filename);

        int iterations = 10 * 1000 * 1000;
        if(args.length > 1) {
            iterations = Integer.parseInt(args[1]);
        }
        System.out.println("iteration: " + iterations);

        long startTime = System.currentTimeMillis();
        for (int i=0; i<iterations; i++) {
            parse(dataCharBuffer);
        }
        long endTime = System.currentTimeMillis();

        long finalTime = endTime - endTime;
        System.out.println("final time: " + finalTime);
    }

    private static void parse(DataCharBuffer dataCharBuffer) {
        for (int j=0; j<dataCharBuffer.length; j++) {
            char theChar = dataCharBuffer.data[j];
        }
    }
}
