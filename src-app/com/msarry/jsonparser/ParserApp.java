package com.msarry.jsonparser;

import com.msarry.jsonparser.core.DataCharBuffer;
import com.msarry.jsonparser.core.IndexBuffer;
import com.msarry.jsonparser.parsers.Parser;
import com.msarry.jsonparser.parsers.Parser2;

import java.io.IOException;
import java.text.DecimalFormat;

public class ParserApp {
    public static void main(String[] args) throws IOException {
        String benchmark = "Parser2";
        if (args.length > 0) {
            benchmark = args[0];
        }
        String filename = "data/medium.json.txt";
        if (args.length > 1) {
            filename = args[1];
        }
        int iterations = 10 * 1000 * 1000;
        if (args.length > 2) {
            iterations = Integer.parseInt(args[2]);
        }
        DecimalFormat format = new DecimalFormat("###,###");
        System.out.println("Benchmark: " + benchmark);
        System.out.println("File: " + filename);
        System.out.println("Iterations: " + iterations);

        DataCharBuffer buffer = FileUtil.readFile(filename);

        long startTime = System.currentTimeMillis();
        if ("Parser".equals(benchmark)) {
            runParserBenchmark(buffer, iterations);
        } else if ("Parser2".equals(benchmark)) {
            runParser2Benchmark(buffer, iterations);
        } else if ("ParserBuilder".equals(benchmark)) {
            runParserBuilderBenchmark(buffer, iterations);
        } else if ("Parser2Builder".equals(benchmark)) {
            runParser2BuilderBenchmark(buffer, iterations);
        } else {
            System.out.println("Benchmark is not recognized");
            return;
        }
        long endTime = System.currentTimeMillis();
        long finalTime = endTime - startTime;
        System.out.println("time: " + format.format(finalTime));
    }

    private static void runParserBenchmark(DataCharBuffer buffer, int iterations) {
        IndexBuffer jsonTokens = new IndexBuffer(8192, true);
        IndexBuffer jsonElements = new IndexBuffer(8192, true);

        Parser jsonParser = new Parser(jsonTokens, jsonElements);

        for (int i = 0; i < iterations; i++) {
            jsonParser.parse(buffer);
        }
    }

    private static void runParser2Benchmark(DataCharBuffer buffer, int iterations) {
        IndexBuffer jsonElements = new IndexBuffer(8192, true);

        Parser2 jsonParser = new Parser2();

        for (int i = 0; i < iterations; i++) {
            jsonParser.parse(buffer, jsonElements);
        }
    }

    private static void runParserBuilderBenchmark(DataCharBuffer buffer, int iterations) {
        IndexBuffer jsonTokens = new IndexBuffer(8192, true);
        IndexBuffer jsonElements = new IndexBuffer(8192, true);

        Parser jsonParser = new Parser(jsonTokens, jsonElements);

        for (int i = 0; i < iterations; i++) {
            jsonParser.parse(buffer);
            JsonObject jsonObject = JsonObjectBuilder.parseJsonObject(buffer, jsonElements);
        }
    }

    private static void runParser2BuilderBenchmark(DataCharBuffer buffer, int iterations) {
        IndexBuffer jsonElements = new IndexBuffer(8192, true);

        Parser2 jsonParser = new Parser2();

        for (int i = 0; i < iterations; i++) {
            jsonParser.parse(buffer, jsonElements);
            JsonObject jsonObject = JsonObjectBuilder.parseJsonObject(buffer, jsonElements);
        }

    }
}
