package com.msarry.jsonparser;

import com.msarry.jsonparser.core.DataCharBuffer;
import com.msarry.jsonparser.core.IndexBuffer;
import com.msarry.jsonparser.parsers.ElementTypes;
import com.msarry.jsonparser.parsers.Navigator;

public class JsonObjectBuilder {
    public static JsonObject parseJsonObject(DataCharBuffer buffer, IndexBuffer elementBuffer) {
        Navigator navigator = new Navigator(buffer, elementBuffer);
        return parseJsonObject(navigator);
    }

    public static JsonObject parseJsonObject(Navigator navigator) {
        JsonObject jsonObject = new JsonObject();

        navigator.next();

        while (navigator.type() != ElementTypes.JSON_OBJECT_END) {
            navigator.next();

            if (navigator.isEqualUnencodedStrings("key")) {
                jsonObject.key = navigator.asString();
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("key2")) {
                jsonObject.key2 = navigator.asInt();
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("key3")) {
                jsonObject.key3 = navigator.asBoolean();
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("stringArray")) {
                navigator.next();
                String[] strings = new String[navigator.countSimpleArrayElements()];
                for (int i=0, n=strings.length; i<n; i++) {
                    strings[i] = navigator.asString();
                    navigator.next();
                }
                jsonObject.stringArray = strings;
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("numberArray")) {
                navigator.next();
                int[] ints = new int[navigator.countSimpleArrayElements()];
                for (int i=0, n=ints.length; i<n; i++) {
                    ints[i] = navigator.asInt();
                    navigator.next();
                }
                jsonObject.numberArray = ints;
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("booleanArray")) {
                navigator.next();
                boolean[] booleans = new boolean[navigator.countSimpleArrayElements()];
                for (int i=0, n=booleans.length; i<n; i++) {
                    booleans[i] = navigator.asBoolean();
                    navigator.next();
                }
                jsonObject.booleanArray = booleans;
                navigator.next();
            } else if (navigator.isEqualUnencodedStrings("sub")) {
                jsonObject.sub = parseJsonObject(navigator);
            }
        }
        navigator.next();

        return jsonObject;
    }
}
