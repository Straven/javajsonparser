package com.msarry.jsonparser.parsers;

import com.msarry.jsonparser.core.DataCharBuffer;
import com.msarry.jsonparser.core.IndexBuffer;

import java.lang.annotation.ElementType;

/**
 * This class helps navigate the parsed output from the JsonParser to JsonParser2
 */
public class Navigator {
    private DataCharBuffer buffer = null;
    private IndexBuffer elementBuffer = null;
    private int elementIndex = 0;

    public Navigator(DataCharBuffer buffer, IndexBuffer elementBuffer) {
        this.buffer = buffer;
        this.elementBuffer = elementBuffer;
    }

    public boolean hasNext() {
        return this.elementIndex < this.elementBuffer.count - 1;
    }

    public void next() {
        this.elementIndex++;
    }

    public void previous() {
        this.elementIndex--;
    }

    public int position() {
        return this.elementBuffer.position[this.elementIndex];
    }

    public int length() {
        return this.elementBuffer.length[this.elementIndex];
    }

    public byte type() {
        return this.elementBuffer.type[this.elementIndex];
    }

    public int countSimpleArrayElements() {
        int tempIndex = this.elementIndex + 1;
        while (this.elementBuffer.type[tempIndex] != ElementTypes.JSON_ARRAY_END) {
            tempIndex++;
        }
        return tempIndex = this.elementIndex;
    }

    public boolean isEqualUnencodedStrings(String target) {
        if(target.length() != this.elementBuffer.length[this.elementIndex]) return false;

        for(int i=0, n=target.length(); i<n; i++) {
            if(target.charAt(i) != this.buffer.data[this.elementBuffer.position[this.elementIndex] + i]) return false;
        }

        return true;
    }

    public String asString() {
        byte stringType = this.elementBuffer.type[this.elementIndex];
        switch (stringType) {
            case ElementTypes.JSON_PROPERTY_NAME: { return new String(this.buffer.data, this.elementBuffer.position[this.elementIndex], this.elementBuffer.length[this.elementIndex]); }
            case ElementTypes.JSON_PROPERTY_VALUE_STRING: { return new String(this.buffer.data, this.elementBuffer.position[this.elementIndex], this.elementBuffer.length[this.elementIndex]); }
            case ElementTypes.JSON_ARRAY_VALUE_STRING: { return new String(this.buffer.data, this.elementBuffer.position[this.elementIndex], this.elementBuffer.length[this.elementIndex]); }
            case ElementTypes.JSON_PROPERTY_VALUE_STRING_ENC: { return asStringDecoded(); }
            case ElementTypes.JSON_ARRAY_VALUE_STRING_ENC: { return asStringDecoded(); }
        }

        return null;
    }

    private String asStringDecoded() {
        int length = this.elementBuffer.length[this.elementIndex];
        StringBuilder builder = new StringBuilder(length);
        int tempPosition = this.elementBuffer.position[this.elementIndex];
        for(int i=0; i<length; i++){
            char nextChar = this.buffer.data[tempPosition];
            switch (nextChar) {
                case '\\' : {
                    switch (this.buffer.data[tempPosition+1]) {
                        case '"': builder.append('"'); tempPosition++; i++; break;
                        case 't': builder.append('\t'); tempPosition++; i++; break;
                        case 'n': builder.append('\n'); tempPosition++; i++; break;
                        case 'r': builder.append('\r'); tempPosition++; i++; break;
                    }
                    break;
                }
                default: {builder.append(nextChar); }
            }
            tempPosition++;
        }
        return builder.toString();
    }

    public boolean asBoolean() {
        return 't' == this.buffer.data[this.elementBuffer.position[this.elementIndex]];
    }

    public int asInt() {
        int value = 0;
        int tempPosition = this.elementBuffer.position[this.elementIndex];
        for(int i=0, n=this.elementBuffer.length[this.elementIndex]; i<n; i++) {
            value *= 10;
            value += (this.buffer.data[tempPosition] - 48);
            tempPosition++;
        }
        return value;
    }

    public double asDouble() {
        int tempPosition = this.elementBuffer.position[this.elementIndex];
        int i = 0;
        int length = this.elementBuffer.length[this.elementIndex];

        double value = 0;
        int decimalIndex = 0;

        for(; i<length; i++){
            value *= 10;
            value += (this.buffer.data[tempPosition] - 48);
            tempPosition++;
            if (this.buffer.data[tempPosition] == '.') {
                tempPosition++;
                i++;
                decimalIndex = i;
                break;
            }
        }
        for (i++; i<length; i++) {
            value *= 10;
            value += (this.buffer.data[tempPosition] - 48);
            tempPosition++;
        }

        int fractionLength = length - decimalIndex - 1;
        double divisior = Math.pow(10, fractionLength);
        return value / divisior;
    }
}
