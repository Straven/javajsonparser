package com.msarry.jsonparser.parsers;

import com.msarry.jsonparser.core.DataCharBuffer;
import com.msarry.jsonparser.core.IndexBuffer;
import com.msarry.jsonparser.core.ParserException;

public class Parser {

    private IndexBuffer tokenBuffer = null;
    private IndexBuffer elementBuffer = null;
    private int elementIndex = 0;
    private Tokenizer tokenizer = null;

    public Parser(IndexBuffer tokenBuffer, IndexBuffer elementBuffer) {
        this.tokenBuffer = tokenBuffer;
        this.tokenizer = new Tokenizer(this.tokenBuffer);
        this.elementBuffer = elementBuffer;
    }

    public void parse(DataCharBuffer dataBuffer) {
        this.elementIndex = 0;
        this.tokenizer.reinitialize(dataBuffer, this.tokenBuffer);
        parseObject(this.tokenizer);
        this.elementBuffer.count = this.elementIndex;
    }

    private void parseObject(Tokenizer tokenizer) {
        assertHasMoreTokens(tokenizer);
        tokenizer.parseToken();
        assertThisTokenType(tokenizer.getTokenType(), TokenTypes.JSON_CURLY_BRACKET_LEFT);
        setElementData(tokenizer, ElementTypes.JSON_OBJECT_START);

        tokenizer.nextToken();
        tokenizer.parseToken();
        byte tokenType = tokenizer.getTokenType();

        while (tokenType != TokenTypes.JSON_CURLY_BRACKET_RIGHT) {
            assertThisTokenType(tokenType, TokenTypes.JSON_STRING_TOKEN);
            setElementData(tokenizer, ElementTypes.JSON_PROPERTY_NAME);

            tokenizer.nextToken();
            tokenizer.parseToken();
            tokenType = tokenizer.getTokenType();
            assertThisTokenType(tokenType, TokenTypes.JSON_COLON);

            tokenizer.nextToken();
            tokenizer.parseToken();
            tokenType = tokenizer.getTokenType();

            switch (tokenType) {
                case TokenTypes.JSON_STRING_TOKEN        : { setElementData(tokenizer, ElementTypes.JSON_PROPERTY_VALUE_STRING);    } break;
                case TokenTypes.JSON_STRING_ENC_TOKEN    : { setElementData(tokenizer, ElementTypes.JSON_PROPERTY_VALUE_STRING_ENC);} break;
                case TokenTypes.JSON_NUMBER_TOKEN        : { setElementData(tokenizer, ElementTypes.JSON_PROPERTY_VALUE_NUMBER);    } break;
                case TokenTypes.JSON_BOOLEAN_TOKEN       : { setElementData(tokenizer, ElementTypes.JSON_PROPERTY_VALUE_BOOLEAN);   } break;
                case TokenTypes.JSON_NULL_TOKEN          : { setElementData(tokenizer, ElementTypes.JSON_PROPERTY_VALUE_NULL);      } break;
                case TokenTypes.JSON_CURLY_BRACKET_LEFT  : { parseObject(tokenizer); } break;
                case TokenTypes.JSON_SQUARE_BRACKET_LEFT : { parseArray(tokenizer); } break;
            }

            tokenizer.nextToken();
            tokenizer.parseToken();
            tokenType = tokenizer.getTokenType();
            if(tokenType == TokenTypes.JSON_COMMA) {
                tokenizer.nextToken();
                tokenizer.parseToken();
                tokenType = tokenizer.getTokenType();
            }
        }
        setElementData(tokenizer, ElementTypes.JSON_OBJECT_END);
    }

    private void assertHasMoreTokens(Tokenizer tokenizer) {
        if (!tokenizer.hasMoreTokens()) {
            throw new ParserException("Expected more tokens available in the tokenizer");
        }
    }

    private final void assertThisTokenType(byte tokenType, byte expectedTokenType) {
        if(tokenType != expectedTokenType) {
            throw new ParserException("Token type mismatch: Expected " + expectedTokenType + " but found " + tokenType);
        }
    }

    private void setElementData(Tokenizer tokenizer, byte elementType) {
        this.elementBuffer.position[this.elementIndex] = tokenizer.getTokenPosition();
        this.elementBuffer.length[this.elementIndex] = tokenizer.getTokenLength();
        this.elementBuffer.type[this.elementIndex] = elementType;
        this.elementIndex++;
    }

    private void parseArray(Tokenizer tokenizer) {
        setElementData(tokenizer, ElementTypes.JSON_ARRAY_START);

        tokenizer.nextToken();
        tokenizer.parseToken();

        while (tokenizer.getTokenType() != TokenTypes.JSON_SQUARE_BRACKET_RIGHT) {
            byte tokenType = tokenizer.getTokenType();
            switch (tokenType) {
                case TokenTypes.JSON_STRING_TOKEN       : { setElementData(tokenizer, ElementTypes.JSON_ARRAY_VALUE_STRING);    } break;
                case TokenTypes.JSON_STRING_ENC_TOKEN   : { setElementData(tokenizer, ElementTypes.JSON_ARRAY_VALUE_STRING_ENC);} break;
                case TokenTypes.JSON_NUMBER_TOKEN       : { setElementData(tokenizer, ElementTypes.JSON_ARRAY_VALUE_NUMBER);    } break;
                case TokenTypes.JSON_BOOLEAN_TOKEN      : { setElementData(tokenizer, ElementTypes.JSON_ARRAY_VALUE_BOOLEAN);   } break;
                case TokenTypes.JSON_NULL_TOKEN         : { setElementData(tokenizer, ElementTypes.JSON_ARRAY_VALUE_NULL);      } break;
                case TokenTypes.JSON_CURLY_BRACKET_LEFT : { parseObject(tokenizer); } break;
            }
            tokenizer.nextToken();
            tokenizer.parseToken();
            tokenType = tokenizer.getTokenType();
            if (tokenType == TokenTypes.JSON_COMMA) {
                tokenizer.nextToken();
                tokenizer.parseToken();
                tokenType = tokenizer.getTokenType();
            }
        }

        setElementData(tokenizer, ElementTypes.JSON_ARRAY_END);
    }
}
