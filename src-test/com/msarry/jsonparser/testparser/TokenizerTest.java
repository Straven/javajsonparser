package com.msarry.jsonparser.testparser;

import com.msarry.jsonparser.core.DataCharBuffer;
import com.msarry.jsonparser.core.IndexBuffer;
import com.msarry.jsonparser.parsers.TokenTypes;
import com.msarry.jsonparser.parsers.Tokenizer;

import org.junit.Assert;
import org.junit.Test;

public class TokenizerTest {
    @Test
    public void test() {
        DataCharBuffer dataBuffer = new DataCharBuffer();
        dataBuffer.data = "{  \"key\" : \"value\" }".toCharArray();
        dataBuffer.length = dataBuffer.data.length;

        Tokenizer tokenizer = new Tokenizer(dataBuffer, new IndexBuffer(dataBuffer.data.length, true));

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.parseToken();
        Assert.assertEquals(0, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_CURLY_BRACKET_LEFT, tokenizer.getTokenType());

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(4, tokenizer.getTokenPosition());
        Assert.assertEquals(3, tokenizer.getTokenLength());

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(9, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_COLON, tokenizer.getTokenType());

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(12, tokenizer.getTokenPosition());
        Assert.assertEquals(5, tokenizer.getTokenLength());

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(19, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_CURLY_BRACKET_RIGHT, tokenizer.getTokenType());

        Assert.assertFalse(tokenizer.hasMoreTokens());
    }

    @Test
    public void testNumbers() {
        DataCharBuffer dataBuffer = new DataCharBuffer();
        dataBuffer.data = "{  \"key\" : 0.123, \"key2\" : 1234567890.0123456789 }".toCharArray();
        dataBuffer.length = dataBuffer.data.length;

        Tokenizer tokenizer = new Tokenizer(dataBuffer, new IndexBuffer(dataBuffer.data.length, true));

        Assert.assertTrue(tokenizer.hasMoreTokens());
        tokenizer.parseToken();

        Assert.assertEquals(0, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_CURLY_BRACKET_LEFT, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(4, tokenizer.getTokenPosition());
        Assert.assertEquals(3, tokenizer.getTokenLength());
        Assert.assertEquals(TokenTypes.JSON_STRING_TOKEN, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(9, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_COLON, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(11, tokenizer.getTokenPosition());
        Assert.assertEquals(5, tokenizer.getTokenLength());
        Assert.assertEquals(TokenTypes.JSON_NUMBER_TOKEN, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(16, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_COMMA, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(19, tokenizer.getTokenPosition());
        Assert.assertEquals(4, tokenizer.getTokenLength());
        Assert.assertEquals(TokenTypes.JSON_STRING_TOKEN, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(25, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_COLON, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(27, tokenizer.getTokenPosition());
        Assert.assertEquals(21, tokenizer.getTokenLength());
        Assert.assertEquals(TokenTypes.JSON_NUMBER_TOKEN, tokenizer.getTokenType());

        tokenizer.nextToken();
        tokenizer.parseToken();
        Assert.assertEquals(49, tokenizer.getTokenPosition());
        Assert.assertEquals(TokenTypes.JSON_CURLY_BRACKET_RIGHT, tokenizer.getTokenType());
    }
}
